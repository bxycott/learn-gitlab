# Stack
* [Javalin](https://javalin.io/) web server
* [Exposed](https://github.com/JetBrains/Exposed) database ORM
* [kotlinx.html](https://github.com/Kotlin/kotlinx.html) HTML DSL