package org.jetbrains.kotlin.demo

import java.net.URL

fun gettext(label: String): String {
    return label
}

object Helpers {
    private val baseUrlString = "http://localhost:8080"
    private val baseUrl = URL(baseUrlString)

    fun getUrl(path: String): URL {
        return URL(baseUrl, path)
    }
}

data class WebPage(
    val name: String,
    val url: URL
)

object Navigation {
    val pages = ArrayList<WebPage>()

    fun addPage(page: WebPage) {
        pages.add(page)
    }
}
