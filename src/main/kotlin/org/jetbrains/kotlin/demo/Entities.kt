package org.jetbrains.kotlin.demo

import org.jetbrains.exposed.sql.Table

object UsersTable : Table() {
    val id = uuid("id").primaryKey()
    val password = text("password")
}

object ClaimTypesTable : Table("claim_types") {
    val id = uuid("id").primaryKey()
    val name = text("name")
}

object ClaimsTable : Table("claims") {
    val id = uuid("id").primaryKey()
    val actor = uuid("actor") references PartiesTable.id
    val target = uuid("target") references PartiesTable.id
    val type = uuid("type") references ClaimTypesTable.id
    val description = text("description")
    val source_ = text("source")
    val happened_at = date("happened_at")
    val created_at = datetime("created_at")
    val updated_at = datetime("updated_at").nullable()
}

object PartiesTable : Table("parties") {
    val id = uuid("id").primaryKey()
    val name = text("name")
}
