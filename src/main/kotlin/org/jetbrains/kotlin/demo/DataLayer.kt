package org.jetbrains.kotlin.demo

import kotlinx.html.div
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.kotlin.demo.pages.claims.*
import org.joda.time.DateTime
import java.util.*

data class Party(
    val id: UUID,
    val name: String
)

data class ClaimType(
    val id: UUID,
    val name: String
)

data class Claim(
    val id: UUID,
    val actor: Party,
    val target: Party,
    val type: ClaimType,
    val description: String,
    val source: String,
    val happened_at: DateTime,
    val created_at: DateTime,
    val updated_at: DateTime?
)

object DataLayer {
    object Parties {
        fun create(party: Party) {
            return transaction {
                PartiesTable.insert {
                    it[id] = party.id
                    it[name] = party.name
                }
            }
        }

        fun delete(id: UUID) {
            return transaction {
                PartiesTable.deleteWhere { PartiesTable.id eq id }
            }
        }

        fun getById(id: UUID): Party {
            return transaction {
                PartiesTable.select({ PartiesTable.id eq id }).first()
            }.let {
                Party(it[PartiesTable.id], it[PartiesTable.name])
            }
        }

        fun getAll(): List<Party> {
            return transaction {
                PartiesTable.selectAll().map {
                    Party(it[PartiesTable.id], it[PartiesTable.name])
                }
            }
        }
    }

    object ClaimTypes {
        fun create(claimType: ClaimType) {
            return transaction {
                ClaimTypesTable.insert {
                    it[id] = claimType.id
                    it[name] = claimType.name
                }
            }
        }

        fun getById(id: UUID): ClaimType {
            return transaction {
                ClaimTypesTable.select({ PartiesTable.id eq id }).first()
            }.let {
                ClaimType(it[ClaimTypesTable.id], it[ClaimTypesTable.name])
            }
        }

        fun getAll(): List<ClaimType> {
            return transaction {
                ClaimTypesTable.selectAll().map {
                    ClaimType(it[ClaimTypesTable.id], it[ClaimTypesTable.name])
                }
            }
        }

        fun getCount(): Int {
            return transaction {
                ClaimTypesTable.selectAll().count()
            }
        }
    }

    object Claims {
        fun getAll(): List<Claim> {
            val partiesAliasActor = PartiesTable.alias("parties_actor")
            val partiesAliasTarget = PartiesTable.alias("parties_target")

            return transaction {
                ClaimsTable
                    .leftJoin(partiesAliasActor, { ClaimsTable.actor }, { partiesAliasActor[PartiesTable.id] })
                    .leftJoin(partiesAliasTarget, { ClaimsTable.target }, { partiesAliasTarget[PartiesTable.id] })
                    .leftJoin(ClaimTypesTable, { ClaimsTable.type }, { ClaimTypesTable.id })
                    .selectAll()
                    .limit(10)
                    .map {
                        Claim(
                            it[ClaimsTable.id],
                            Party(
                                it[partiesAliasActor[PartiesTable.id]],
                                it[partiesAliasActor[PartiesTable.name]]
                            ),
                            Party(
                                it[partiesAliasTarget[PartiesTable.id]],
                                it[partiesAliasTarget[PartiesTable.name]]
                            ),
                            ClaimType(it[ClaimTypesTable.id], it[ClaimTypesTable.name]),
                            it[ClaimsTable.description],
                            it[ClaimsTable.source_],
                            it[ClaimsTable.happened_at],
                            it[ClaimsTable.created_at],
                            it[ClaimsTable.updated_at]
                        )
                    }
            }
        }

        fun create(
            actor: String,
            target: String,
            type: UUID,
            source: String,
            description: String,
            happened_at: DateTime
        ) {
            transaction {
                val actorEntity = PartiesTable.select({ PartiesTable.name eq actor }).firstOrNull()
                val targetEntity = PartiesTable.select({ PartiesTable.name eq target }).firstOrNull()

                var actorId: UUID
                var targetId: UUID

                if (actorEntity == null) {
                    val newActorId = UUID.randomUUID()

                    PartiesTable.insert {
                        it[id] = newActorId
                        it[name] = actor
                    }

                    actorId = newActorId
                } else {
                    actorId = actorEntity[PartiesTable.id]
                }

                if (targetEntity == null) {
                    val newTargetId = UUID.randomUUID()

                    PartiesTable.insert {
                        it[id] = newTargetId
                        it[name] = target
                    }

                    targetId = newTargetId
                } else {
                    targetId = targetEntity[PartiesTable.id]
                }

                ClaimsTable.insert {
                    it[id] = UUID.randomUUID()
                    it[ClaimsTable.actor] = actorId
                    it[ClaimsTable.target] = targetId
                    it[ClaimsTable.type] = type
                    it[ClaimsTable.description] = description
                    it[source_] = source
                    it[ClaimsTable.happened_at] = happened_at
                    it[created_at] = DateTime.now()
                }
            }
        }
    }
}
