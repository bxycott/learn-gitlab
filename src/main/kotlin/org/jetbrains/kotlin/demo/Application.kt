package org.jetbrains.kotlin.demo

import io.javalin.Javalin
import io.javalin.http.staticfiles.Location
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.kotlin.demo.pages.claim_types.registerClaimTypePages
import org.jetbrains.kotlin.demo.pages.claims.registerClaimsPages
import org.jetbrains.kotlin.demo.pages.claims.viewClaims
import org.jetbrains.kotlin.demo.pages.notFoundPage
import org.jetbrains.kotlin.demo.pages.parties.registerPartiesPages

fun main(args: Array<String>) {
    val DATABASE_URL = System.getenv("DATABASE_URL")

    val withoutPostrgrePrefix = DATABASE_URL.replace("postgres://", "")
    val credentialsAndDbUrl = withoutPostrgrePrefix.split("@")
    val DB_USER = credentialsAndDbUrl[0].split(":")[0]
    val DB_PASSWORD = credentialsAndDbUrl[0].split(":")[1]
    val DB_PATH = credentialsAndDbUrl[1]

    Class.forName("org.postgresql.Driver")
    Database.connect("jdbc:postgresql://$DB_PATH", driver = "org.postgresql.Driver", user = DB_USER, password = DB_PASSWORD)

    transaction {
        SchemaUtils.create(UsersTable, ClaimTypesTable, PartiesTable, ClaimsTable)
    }

    val app = Javalin.create()

    app.config.addStaticFiles("static", Location.EXTERNAL)

    app.start(System.getenv("PORT")?.toInt() ?: 8080)

    Navigation.addPage(WebPage(gettext("Home"), Helpers.getUrl("/")))
    app.get("/", ::viewClaims)

    registerClaimTypePages(app)
    registerPartiesPages(app)
    registerClaimsPages(app)

    app.error(404, ::notFoundPage)
}
