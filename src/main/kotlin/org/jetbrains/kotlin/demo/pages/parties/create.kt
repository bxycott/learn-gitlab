package org.jetbrains.kotlin.demo.pages.parties

import io.javalin.http.Context
import kotlinx.html.*
import org.jetbrains.kotlin.demo.*
import org.jetbrains.kotlin.demo.pages.Body
import org.jetbrains.kotlin.demo.pages.Head
import org.jetbrains.kotlin.demo.pages.Page
import org.jetbrains.kotlin.demo.pages.errorPage
import java.util.*

val nameField = "name"

fun partyCreate(ctx: Context) {
    ctx.html(
        Page {
            Head {
                title {
                    + gettext("Untitled")
                }
            }

            Body {
                form {
                    method = FormMethod.post
                    action = "${Urls.Parties.create}"

                    input {
                        type = InputType.text
                        name = nameField
                    }

                    button {
                        type = ButtonType.submit

                        + gettext("Submit")
                    }
                }
            }
        }
    )
}

fun partyCreateHandler(ctx: Context) {
    val formName = ctx.formParam(org.jetbrains.kotlin.demo.pages.claim_types.nameField)

    if (formName == null) {
        ctx.html(errorPage())
        return
    }

    DataLayer.Parties.create(
        Party(UUID.randomUUID(), formName)
    )

    ctx.redirect("${Urls.Parties.view}")
}
