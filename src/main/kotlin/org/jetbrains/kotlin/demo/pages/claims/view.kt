package org.jetbrains.kotlin.demo.pages.claims

import io.javalin.http.Context
import kotlinx.html.*
import org.jetbrains.kotlin.demo.DataLayer
import org.jetbrains.kotlin.demo.gettext
import org.jetbrains.kotlin.demo.pages.Body
import org.jetbrains.kotlin.demo.pages.Head
import org.jetbrains.kotlin.demo.pages.Page

fun viewClaims(ctx: Context) {
    val claims = DataLayer.Claims.getAll()

    ctx.html(
        Page {
            Head {
                title {
                    + gettext("Untitled")
                }
            }

            Body {
                h1 {
                    + gettext("Claims")
                }

                div {
                    a {
                        href = "${Urls.Claims.add}"

                        + gettext("Add new")
                    }

                    br {}
                    br {}
                }

                table {
                    classes = setOf("app-table")

                    thead {
                        tr {
                            th { + gettext("Actor") }
                            th { + gettext("Target") }
                            th { + gettext("Action type") }
                            th { + gettext("Description") }
                            th { + gettext("Link") }
                            th { + gettext("Happened at") }
                        }
                    }
                    tbody {
                        for (claim in claims) {
                            tr {
                                td { + claim.actor.name }
                                td { + claim.target.name }
                                td { + claim.type.name }
                                td { + claim.description }
                                td {
                                    a {
                                        href = claim.source
                                        + gettext("source")
                                    }
                                }
                                td {
                                    input {
                                        type = InputType.date
                                        value = claim.happened_at.toString("yyyy-MM-dd")
                                        readonly = true
                                        required = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    )
}
