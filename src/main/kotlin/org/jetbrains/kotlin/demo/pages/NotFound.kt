package org.jetbrains.kotlin.demo.pages

import io.javalin.http.Context
import kotlinx.html.h1
import kotlinx.html.title
import org.jetbrains.kotlin.demo.gettext

fun notFoundPage(ctx: Context) {
    ctx.html(
        Page {
            Head {
                title {
                    + gettext("404")
                }
            }
            Body {
                h1 {
                    + gettext("Not found")
                }
            }
        }
    )
}
