package org.jetbrains.kotlin.demo.pages.claims

import io.javalin.http.Context
import kotlinx.html.*
import org.jetbrains.kotlin.demo.*
import org.jetbrains.kotlin.demo.pages.Body
import org.jetbrains.kotlin.demo.pages.Head
import org.jetbrains.kotlin.demo.pages.Page
import org.jetbrains.kotlin.demo.pages.errorPage
import org.joda.time.DateTime
import java.util.*

const val actorField = "actor"
const val targetField = "target"
const val typeField = "type"
const val descriptionField = "description"
const val sourceField = "source"
const val happenedAtField = "happenedAt"

const val partiesListId = "partiesList"

fun claimCreateForm(ctx: Context) {
    ctx.html(
        Page {
            Head {
                title {
                    + gettext("Untitled")
                }
            }

            Body {
                if (DataLayer.ClaimTypes.getCount() < 1) {
                    div {
                        + gettext("There are no claims types registered.")
                    }
                } else {
                    form {
                        method = FormMethod.post
                        action = "${Urls.Claims.add}"

                        dataList {
                            id = partiesListId

                            for (party in DataLayer.Parties.getAll()) {
                                option {
                                    + party.name
                                }
                            }
                        }

                        div {
                            label {
                                + gettext("Actor")

                                input {
                                    name = actorField
                                    autoComplete = false
                                    list = partiesListId
                                    required = true
                                }
                            }
                        }

                        div {
                            label {
                                + gettext("Target")

                                input {
                                    name = targetField
                                    autoComplete = false
                                    list = partiesListId
                                    required = true
                                }
                            }
                        }

                        div {
                            label {
                                + gettext("Type")

                                select {
                                    name = typeField
                                    required = true

                                    for (claimType in DataLayer.ClaimTypes.getAll()) {
                                        option {
                                            value = claimType.id.toString()

                                            + claimType.name
                                        }
                                    }
                                }
                            }
                        }

                        div {
                            label {
                                + gettext("Description")

                                textArea {
                                    name = descriptionField
                                    required = true
                                }
                            }
                        }

                        div {
                            label {
                                + gettext("Source")

                                input {
                                    type = InputType.text
                                    name = sourceField
                                    required = true
                                }
                            }
                        }

                        div {
                            label {
                                + gettext("Date")

                                input {
                                    type = InputType.date
                                    name = happenedAtField
                                    required = true
                                }
                            }
                        }

                        button {
                            type = ButtonType.submit

                            + gettext("Submit")
                        }
                    }
                }
            }
        }
    )
}

fun claimCreateFormHandler(ctx: Context) {
    val actorValue = ctx.formParam(actorField)
    val targetValue = ctx.formParam(targetField)
    val typeValue = UUID.fromString(ctx.formParam(typeField))
    val sourceValue = ctx.formParam(sourceField)
    val descriptionValue = ctx.formParam(descriptionField)
    val happenedAtValue = ctx.formParam(happenedAtField)

    if (sourceValue == null || actorValue == null || targetValue == null || descriptionValue == null || happenedAtValue == null) {
        ctx.html(errorPage())
        return
    }

    DataLayer.Claims.create(
        actorValue,
        targetValue,
        typeValue,
        sourceValue,
        descriptionValue,
        DateTime.parse(happenedAtValue)
    )

    ctx.redirect("${Urls.Claims.index}")
}
