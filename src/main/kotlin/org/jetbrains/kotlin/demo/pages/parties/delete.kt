package org.jetbrains.kotlin.demo.pages.parties

import io.javalin.http.Context
import org.jetbrains.kotlin.demo.*
import org.jetbrains.kotlin.demo.pages.errorPage
import java.util.*

val idField = "id"

fun partyDeleteHandler(ctx: Context) {
    val id = ctx.formParam(idField)

    if (id == null) {
        ctx.html(errorPage())
        return
    }

    DataLayer.Parties.delete(UUID.fromString(id))

    ctx.redirect("${Urls.Parties.view}")
}

