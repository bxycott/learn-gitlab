package org.jetbrains.kotlin.demo.pages

import kotlinx.html.*
import kotlinx.html.stream.appendHTML
import org.jetbrains.kotlin.demo.Helpers
import org.jetbrains.kotlin.demo.Navigation
import org.jetbrains.kotlin.demo.gettext

fun Page(block: HTML.() -> Unit): String {
    return buildString {
        appendln("<!DOCTYPE html>")
        appendHTML().html {
            block()
        }
    }
}

fun HTML.Head(block: HEAD.() -> Unit) {
    return head {
        meta {
            charset = "utf-8"
        }

        meta {
            name = "viewport"
            content = "width=device-width, initial-scale=1.0"
        }

        link {
            rel = "stylesheet"
            href = "${Helpers.getUrl("assets/app.css")}"
        }

        block()
    }
}

fun HTML.Body(block: BODY.() -> Unit) {
    return body {
        div {
            classes = setOf("app-container")

            NavigationMenu()

            this@body.block()
        }
    }
}

fun DIV.NavigationMenu() {
    ul {
        classes = setOf("app-navigation")

        for (page in Navigation.pages) {
            li {
                a {
                    href = "${page.url}"

                    + page.name
                }
            }
        }
    }
}

fun errorPage(): String {
    return Page {
        Head {
            title {
                + gettext("Error")
            }
        }
        Body {
            h1 {
                + gettext("Unknown error occurred.")
            }
        }
    }
}
