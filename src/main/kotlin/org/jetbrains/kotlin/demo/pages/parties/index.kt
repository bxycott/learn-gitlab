package org.jetbrains.kotlin.demo.pages.parties

import io.javalin.Javalin
import org.jetbrains.kotlin.demo.Helpers
import org.jetbrains.kotlin.demo.Navigation
import org.jetbrains.kotlin.demo.WebPage
import org.jetbrains.kotlin.demo.gettext

object Urls {
    object Parties {
        val create = Helpers.getUrl("parties/create")
        val view = Helpers.getUrl("parties/view")
        val delete = Helpers.getUrl("parties/delete")
    }
}

fun registerPartiesPages(app: Javalin) {
    Navigation.addPage(WebPage(gettext("Parties"), Urls.Parties.view))

    app.get(Urls.Parties.view.path, ::viewParties)
    app.get(Urls.Parties.create.path, ::partyCreate)
    app.post(Urls.Parties.create.path, ::partyCreateHandler)
    app.post(Urls.Parties.delete.path, ::partyDeleteHandler)
}
