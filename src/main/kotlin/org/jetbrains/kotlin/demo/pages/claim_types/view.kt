package org.jetbrains.kotlin.demo.pages.claim_types

import io.javalin.http.Context
import kotlinx.html.*
import org.jetbrains.kotlin.demo.DataLayer
import org.jetbrains.kotlin.demo.gettext
import org.jetbrains.kotlin.demo.pages.Body
import org.jetbrains.kotlin.demo.pages.Head
import org.jetbrains.kotlin.demo.pages.Page

fun claimTypesView(ctx: Context) {
    val claimTypes = DataLayer.ClaimTypes.getAll()

    ctx.html(
        Page {
            Head {
                title {
                    + "Untitled"
                }
            }

            Body {
                h1 {
                    + gettext("Claim types")
                }

                div {
                    a {
                        href = "${Urls.ClaimTypes.create}"

                        + gettext("Add new")
                    }

                    br {}
                    br {}
                }

                table {
                    classes = setOf("app-table")

                    thead {
                        tr {
                            th {
                                + gettext("ID")
                            }

                            th {
                                + gettext("Name")
                            }
                        }
                    }
                    tbody {
                        for (claimType in claimTypes) {
                            tr {
                                td { + claimType.id.toString() }
                                td { + claimType.name }
                            }
                        }
                    }
                }
            }
        }
    )
}
