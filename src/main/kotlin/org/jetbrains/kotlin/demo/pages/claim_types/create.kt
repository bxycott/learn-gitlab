package org.jetbrains.kotlin.demo.pages.claim_types

import io.javalin.http.Context
import kotlinx.html.*
import org.jetbrains.kotlin.demo.ClaimType
import org.jetbrains.kotlin.demo.DataLayer
import org.jetbrains.kotlin.demo.gettext
import org.jetbrains.kotlin.demo.pages.Body
import org.jetbrains.kotlin.demo.pages.Head
import org.jetbrains.kotlin.demo.pages.Page
import org.jetbrains.kotlin.demo.pages.errorPage
import java.util.*

const val nameField = "name"

fun claimTypeCreate(ctx: Context) {
    ctx.html(
        Page {
            Head {
                title {
                    + gettext("Untitled")
                }
            }

            Body {
                form {
                    method = FormMethod.post
                    action = "${Urls.ClaimTypes.create}"

                    input {
                        type = InputType.text
                        name = nameField
                    }

                    button {
                        type = ButtonType.submit

                        + gettext("Submit")
                    }
                }
            }
        }
    )
}

fun claimTypeCreateHandler(ctx: Context) {
    val formName = ctx.formParam(nameField)

    if (formName == null) {
        ctx.html(errorPage())
        return
    }

    DataLayer.ClaimTypes.create(
        ClaimType(UUID.randomUUID(), formName)
    )

    ctx.redirect("${Urls.ClaimTypes.view}")
}
