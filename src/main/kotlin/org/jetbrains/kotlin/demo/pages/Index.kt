package org.jetbrains.kotlin.demo.pages

import io.javalin.http.Context
import kotlinx.html.*
import org.jetbrains.kotlin.demo.gettext

fun indexPage(ctx: Context) {
    ctx.html(
        Page {
            Head {
                title {
                    + gettext("Untitled")
                }

                link {
                    rel = "stylesheet"
                    type = "text/css"
                    href = "/assets/style.css"
                }
            }

            Body {

                div {
                    classes = setOf("wrapper")

                    + gettext("hello")
                }
            }
        }
    )
}
