package org.jetbrains.kotlin.demo.pages.claim_types

import io.javalin.Javalin
import org.jetbrains.kotlin.demo.*

object Urls {
    object ClaimTypes {
        val view = Helpers.getUrl("claim-types")
        val create = Helpers.getUrl("claim-types/create")
    }
}

fun registerClaimTypePages(app: Javalin) {
    Navigation.addPage(WebPage(gettext("Claim types"), Urls.ClaimTypes.view))

    app.get(Urls.ClaimTypes.view.path, ::claimTypesView)
    app.get(Urls.ClaimTypes.create.path, ::claimTypeCreate)
    app.post(Urls.ClaimTypes.create.path, ::claimTypeCreateHandler)
}
