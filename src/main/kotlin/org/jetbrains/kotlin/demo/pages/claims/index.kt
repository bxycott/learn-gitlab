package org.jetbrains.kotlin.demo.pages.claims

import io.javalin.Javalin
import org.jetbrains.kotlin.demo.Helpers
import org.jetbrains.kotlin.demo.Navigation
import org.jetbrains.kotlin.demo.WebPage
import org.jetbrains.kotlin.demo.gettext

object Urls {
    object Claims {
        val index = Helpers.getUrl("claims")
        val add = Helpers.getUrl("claims/add")
    }
}

fun registerClaimsPages(app: Javalin) {
    Navigation.addPage(WebPage(gettext("Claims"), Urls.Claims.index))

    app.get(Urls.Claims.index.path, ::viewClaims)
    app.get(Urls.Claims.add.path, ::claimCreateForm)
    app.post(Urls.Claims.add.path, ::claimCreateFormHandler)
}
