* Remove "success" and redirect to a next page instead.
* Add navigation to a header.
* Use lazy autocomplete for picking parties.